
import React from 'react';
import TodoList from './todo-list';
import 'antd/dist/antd.css';
import './App.css';

function App() {
  return (
    <div className="App">
      <TodoList />
    </div>
  );
}

export default App;
