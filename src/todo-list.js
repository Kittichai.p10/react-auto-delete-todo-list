import React, { useState, useEffect } from 'react';
import { Input, Button, Row } from 'antd';
import './todo-list.css';

export default function TodoList() {

    const [todo, setTodo] = useState('');
    const [todos, setTodos] = useState([]);

    useEffect(() => {
        if (todos.length > 0) {
            let count = todos.length - 1;
            const interval = setInterval(() => {
                let updatedTodos = [...todos].filter((todo, index) => index !== count);
                setTodos(updatedTodos);
            }, 5000);
            return () => clearInterval(interval);
        }
    }, [todos]);

    const handleAddTodo = () => {
        const newTodo = {
            id: new Date().getTime(),
            text: todo,
            completed: false,
        };
        setTodos([...todos].concat(newTodo));
        setTodo("");
    }

    return (
        <div className='todo-list'>
            <Input className='todo-input' onChange={e => setTodo(e.target.value)} value={todo} />
            <Button type="primary" className='add-todo'onClick={handleAddTodo}>Add Todo</Button>
            <div className='todo-lists'>
                {todos.map(item => (
                    <div className='todo-list-text' key={item.id}>{item.text}</div>
                ))}
            </div>
        </div>
    )
}
